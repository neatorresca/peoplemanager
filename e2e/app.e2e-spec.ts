import { PeoplemanagerPage } from './app.po';

describe('peoplemanager App', () => {
  let page: PeoplemanagerPage;

  beforeEach(() => {
    page = new PeoplemanagerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
