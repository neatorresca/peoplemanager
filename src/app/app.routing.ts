import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {ListarClientesComponent} from './components/listar-clientes.component';
import {RegistrarClientesComponent} from './components/registrar-clientes.component';
import {DetalleClienteComponent} from './components/detalle-cliente.component';
import {ActualizarClientesComponent} from './components/actualizar-clientes.component';

const appRoutes: Routes = [
  {path:'',redirectTo:'/clientes', pathMatch:'full'},
  {path:'clientes',component:ListarClientesComponent},
  {path:'registro',component:RegistrarClientesComponent},
  {path:'clientes/:id',component:DetalleClienteComponent},
  {path:'clientes/:id/actualizar',component:ActualizarClientesComponent}
]

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);