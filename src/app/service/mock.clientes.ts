import {Persona} from '../model/Persona';

export const CLIENTES: Persona[] = [
    new Persona(1,"Neiro Torres",1234568),
    new Persona(2,"Eliza Restrepo",1234569),
    new Persona(3,"Depy Fap",1234566),
    new Persona(4,"Simon Croteau",1234567),
    new Persona(5,"Alguien más",1234565)];