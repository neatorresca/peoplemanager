import { Injectable } from '@angular/core';
import {Http,Response,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { Persona } from '../model/persona';

@Injectable()
export class ClientesService {
	constructor(private _http:Http){}
	
	getClientes(){
		return this._http.get('http://localhost:3000/clients')
			.map(res => res.json());
	}

	getClient(id:number){
		return this._http.get('http://localhost:3000/clients/' + id)
			.map(res=>res.json());
	}

	addCliente(persona:Persona){
		let json = JSON.stringify(persona);
		/*let params = "json="+json;*/
		let headers = new Headers({"Content-Type":"application/json"});

		return this._http.post("http://localhost:3000/clients",json,{headers:headers})
			.map(res => res.json());
	}

	modifyCliente(persona:Persona){
		let json = JSON.stringify(persona);
		let headers = new Headers({"Content-Type":"application/json"});
		return this._http.put("http://localhost:3000/clients/" + persona.id,json,{headers})
			.map(res => res.json());
	}

	deleteCliente(id:number){
		return this._http.delete("http://localhost:3000/clients/" + id)
			.map(res=> res.json())
	}
}