import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './view/home.html'
})
export class AppComponent {
	title = 'SISTEMA GESTOR DE CLIENTES!';
}
