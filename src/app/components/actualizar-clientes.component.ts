import { Component,OnInit } from '@angular/core';
import { ClientesService } from '../service/clientes.service';
import { Persona } from '../model/persona';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
	selector: 'clients-update',
	templateUrl: '../view/registrar-clientes.html'
})

export class ActualizarClientesComponent implements OnInit {
	public cliente:Persona = new Persona(null,"",0);
	constructor(private _clientsService: ClientesService, private _route: ActivatedRoute, private _router: Router){
	}

	public _id;

	ngOnInit(){
		this._route.params.forEach((params: Params) => {
			this._id = params['id'];
		});
		this._clientsService.getClient(this._id)
			.subscribe(res=>this.cliente = res.client)
	}

	onSubmit(){
		this._clientsService.modifyCliente(this.cliente)
			.subscribe(res=>this.cliente = res.client)
	}
}