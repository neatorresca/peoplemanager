import { Component,OnInit } from '@angular/core';
import { ClientesService } from '../service/clientes.service';
import { Persona } from '../model/persona';

@Component({
	selector: 'client-register',
	templateUrl: '../view/registrar-clientes.html'
})

export class RegistrarClientesComponent implements OnInit {
	public cliente:Persona = new Persona(null,"",0);
	public enviado:boolean;
	public status;
	public errorMessage;

	constructor(private _clientesService:ClientesService){}
	ngOnInit(){
		this.enviado=false;
	}

	onSubmit(){
		this._clientesService.addCliente(this.cliente)
			.subscribe(
				result=>{
					this.status = result.status;
					this.cliente = result.client;
				},
				error=>{
					this.errorMessage = <any>error;
				}
			)
		this.enviado=true;
	}
}