import { Component,OnInit } from '@angular/core';
import { ClientesService } from '../service/clientes.service';
import { Persona } from '../model/persona';
/*import 'rxjs/add/operator/toPromise';*/

@Component({
	selector: 'client-list',
	templateUrl: '../view/clientes.html'
})

export class ListarClientesComponent implements OnInit {
	public clientes:Persona[];
	public status;
	public clientDetail:Persona = undefined;

	constructor(private _clientesService:ClientesService){}

	ngOnInit(){
		this.getClientes();
	}

	getClientes(){
		this._clientesService.getClientes()
			.subscribe(
				result => {
					this.clientes = result.clients;
					this.status = result;
				},
				error => {
					console.log(error);
					console.log("error en el servidor");
				}
			)
	}

	deleteElement(id:number){
		this._clientesService.deleteCliente(id)
			.subscribe(
				result => {
					this.getClientes();
				},
				error => {
					console.log(error);
					console.log("Error en el server");
				}
			)
	}

	getClient(id: number){
		this._clientesService.getClient(id).subscribe(
			data => this.clientDetail = data.client
		)
	}
}