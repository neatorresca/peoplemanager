import { Component } from '@angular/core';
import { ClientesService } from '../service/clientes.service';
import { Persona } from '../model/persona';

@Component({
	selector: 'client-detail',
	templateUrl: '../view/detalle-cliente.html'
})

export class DetalleClienteComponent {
	public title = 'Vista detalle cliente'
}