import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders } from './app.routing';

//SERVICES
import { ClientesService } from './service/clientes.service';
    
import { AppComponent } from './app.component';
import { ActualizarClientesComponent } from './components/actualizar-clientes.component';
import { DetalleClienteComponent } from './components/detalle-cliente.component';
import { ListarClientesComponent } from './components/listar-clientes.component';
import { RegistrarClientesComponent } from './components/registrar-clientes.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarClientesComponent,
    RegistrarClientesComponent,
    DetalleClienteComponent,
    ActualizarClientesComponent
  ],
  imports: [routing,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [appRoutingProviders,ClientesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
