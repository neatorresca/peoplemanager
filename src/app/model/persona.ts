export class Persona{
	constructor(
		public id:number,
		public name:string,
		public phone:number){}
}